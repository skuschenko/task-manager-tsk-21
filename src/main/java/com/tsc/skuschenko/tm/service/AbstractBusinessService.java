package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.IBusinessRepository;
import com.tsc.skuschenko.tm.api.service.IBusinessService;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.empty.EmptyIdException;
import com.tsc.skuschenko.tm.exception.empty.EmptyNameException;
import com.tsc.skuschenko.tm.exception.empty.EmptyStatusException;
import com.tsc.skuschenko.tm.exception.entity.EntityNotFoundException;
import com.tsc.skuschenko.tm.exception.system.IndexIncorrectException;
import com.tsc.skuschenko.tm.model.AbstractBusinessEntity;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Optional;

public class AbstractBusinessService<E extends AbstractBusinessEntity>
        extends AbstractService<E> implements IBusinessService<E> {

    IBusinessRepository<E> entityRepository;

    public AbstractBusinessService(
            final IBusinessRepository<E> entityRepository
    ) {
        super(entityRepository);
        this.entityRepository = entityRepository;
    }

    @Override
    public E changeStatusById(
            final String userId, final String id, final Status status
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E changeStatusByIndex(
            final String userId, final Integer index, final Status status
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        final E entity = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setStatus(status);
        return entity;
    }

    @Override
    public E changeStatusByName(
            final String userId, final String name, final Status status
    ) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        Optional.ofNullable(status).orElseThrow(EmptyStatusException::new);
        final E entity = Optional.ofNullable(findOneByName(userId, name))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setStatus(status);
        return entity;
    }

    @Override
    public void clear(String userId) {
        entityRepository.clear(userId);
    }

    @Override
    public E completeById(final String userId, final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E completeByIndex(final String userId, final Integer index) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        final E entity = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public E completeByName(final String userId, final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        final E entity = Optional.ofNullable(findOneByName(userId, name))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setStatus(Status.COMPLETE);
        entity.setDateFinish(new Date());
        return entity;
    }

    @Override
    public List<E> findAll(
            final String userId, final Comparator<E> comparator
    ) {
        Optional.ofNullable(comparator).orElse(null);
        return entityRepository.findAll(userId, comparator);
    }

    @Override
    public List<E> findAll(String userId) {
        return entityRepository.findAll(userId);
    }

    @Override
    public E findOneById(final String userId, String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.findOneById(userId, id);
    }

    @Override
    public E findOneByIndex(final String userId, final Integer index) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        return entityRepository.findOneByIndex(userId, index);
    }

    @Override
    public E findOneByName(final String userId, final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return entityRepository.findOneByName(userId, name);
    }

    @Override
    public E removeOneById(final String userId, final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        return entityRepository.removeOneById(userId, id);
    }

    @Override
    public E removeOneByIndex(final String userId, final Integer index) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        return entityRepository.removeOneByIndex(userId, index);
    }

    @Override
    public E removeOneByName(final String userId, final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        return entityRepository.removeOneByName(userId, name);
    }

    @Override
    public E startById(final String userId, final String id) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E startByIndex(final String userId, final Integer index) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        final E entity = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E startByName(final String userId, final String name) {
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        final E entity = Optional.ofNullable(findOneByName(userId, name))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setStatus(Status.IN_PROGRESS);
        entity.setDateStart(new Date());
        return entity;
    }

    @Override
    public E updateOneById(
            final String userId, final String id, final String name,
            final String description
    ) {
        Optional.ofNullable(id).orElseThrow(EmptyIdException::new);
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        final E entity = Optional.ofNullable(findOneById(userId, id))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

    @Override
    public E updateOneByIndex(
            final String userId, final Integer index, final String name,
            final String description
    ) {
        Optional.ofNullable(index).filter(item -> item > -1).
                orElseThrow(() -> new IndexIncorrectException(index));
        Optional.ofNullable(name).orElseThrow(EmptyNameException::new);
        final E entity = Optional.ofNullable(findOneByIndex(userId, index))
                .orElseThrow(() ->
                        new EntityNotFoundException(
                                this.getClass().getSimpleName()
                        )
                );
        entity.setName(name);
        entity.setDescription(description);
        return entity;
    }

}
