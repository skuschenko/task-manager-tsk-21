package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.command.AbstractCommand;

import java.util.*;
import java.util.stream.Collectors;

public class CommandRepository implements ICommandRepository {

    private final Map<String, AbstractCommand> arguments =
            new LinkedHashMap<>();

    private final Map<String, AbstractCommand> commands =
            new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand abstractCommand) {
        final Optional<String> arg =
                Optional.ofNullable(abstractCommand.arg());
        final Optional<String> name =
                Optional.ofNullable(abstractCommand.name());
        arg.ifPresent(item -> arguments.put(item, abstractCommand));
        name.ifPresent(item -> commands.put(item, abstractCommand));
    }

    @Override
    public Collection<AbstractCommand> getArguments() {
        return new ArrayList<>(arguments.values());
    }

    @Override
    public Collection<String> getCommandArgs() {
        return commands.values()
                .stream()
                .map(AbstractCommand::arg)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public AbstractCommand getCommandByArg(final String name) {
        return arguments.get(name);
    }

    @Override
    public AbstractCommand getCommandByName(final String name) {
        return commands.get(name);
    }

    @Override
    public Collection<String> getCommandNames() {
        return commands.values()
                .stream()
                .map(AbstractCommand::name)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @Override
    public Collection<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

}
