package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.IUserRepository;
import com.tsc.skuschenko.tm.model.User;

public class UserRepository extends AbstractRepository<User>
        implements IUserRepository {

    @Override
    public User findByEmail(final String email) {
        return entities.stream()
                .filter(item -> email.equals(item.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByLogin(final String login) {
        return entities.stream()
                .filter(item -> login.equals(item.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User removeByLogin(final String login) {
        final User user = findByLogin(login);
        entities.remove(user);
        return user;
    }

}
