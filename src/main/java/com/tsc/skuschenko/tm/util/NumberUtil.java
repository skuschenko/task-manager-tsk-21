package com.tsc.skuschenko.tm.util;

public interface NumberUtil {

    static String formatSize(final long bytes) {
        final String[] units = {"B", "kB", "MB", "GB", "TB", "PB", "EB"};
        final int base = 1024;
        if (bytes < base) {
            return bytes + " " + units[0];
        }
        final int exponent = (int) (Math.log(bytes) / Math.log(base));
        final String unit = units[exponent];
        return String.format("%.2f %s", bytes / Math.pow(base, exponent), unit);
    }

}
