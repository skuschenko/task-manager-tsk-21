package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ITaskService;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.Optional;

public class TaskUpdateByIndexCommand extends AbstractTaskCommand {

    private final String DESCRIPTION = "update task by index";

    private final String NAME = "task-update-by-index";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("index");
        final Integer valueIndex = TerminalUtil.nextNumber() - 1;
        final ITaskService taskService
                = serviceLocator.getTaskService();
        Task task = taskService.findOneByIndex(userId, valueIndex);
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showParameterInfo("name");
        final String valueName = TerminalUtil.nextLine();
        showParameterInfo("description");
        final String valueDescription = TerminalUtil.nextLine();
        task = taskService.updateOneByIndex(
                userId, valueIndex, valueName, valueDescription
        );
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

    @Override
    public String name() {
        return NAME;
    }

}