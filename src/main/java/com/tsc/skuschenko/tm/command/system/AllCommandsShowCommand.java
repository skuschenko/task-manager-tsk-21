package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;

import java.util.Collection;
import java.util.Optional;

public class AllCommandsShowCommand extends AbstractCommand {

    private final String DESCRIPTION = "commands";

    private final String NAME = "commands";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        final Collection<String> names =
                serviceLocator.getCommandService().getListCommandNames();
        names.stream().filter(item -> Optional.ofNullable(item).isPresent())
                .forEach(System.out::println);
    }

    @Override
    public String name() {
        return NAME;
    }

}
