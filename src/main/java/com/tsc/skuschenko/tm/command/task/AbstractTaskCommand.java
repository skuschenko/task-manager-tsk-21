package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.model.Task;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.Optional;

public abstract class AbstractTaskCommand extends AbstractCommand {

    protected Status readTaskStatus() {
        showParameterInfo("status");
        System.out.println(Arrays.toString(Status.values()));
        final String statusId = TerminalUtil.nextLine();
        final Status status = Status.valueOf(statusId);
        return status;
    }

    protected void showTask(final Task task) {
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        if (task == null) throw new TaskNotFoundException();
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + task.getStatus().getDisplayName());
        System.out.println("START DATE: " + task.getDateStart());
        System.out.println("END DATE: " + task.getDateFinish());
    }

}
