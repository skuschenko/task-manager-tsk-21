package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.IProjectService;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.model.Project;
import com.tsc.skuschenko.tm.util.TerminalUtil;

import java.util.Optional;

public class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    private final String DESCRIPTION = "remove project by name";

    private final String NAME = "project-remove-by-name";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        String userId = serviceLocator.getAuthService().getUserId();
        showOperationInfo(NAME);
        showParameterInfo("name");
        final String value = TerminalUtil.nextLine();
        final IProjectService projectService
                = serviceLocator.getProjectService();
        final Project project = projectService.removeOneByName(userId, value);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
    }

    @Override
    public String name() {
        return NAME;
    }

}
