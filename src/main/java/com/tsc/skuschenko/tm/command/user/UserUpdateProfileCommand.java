package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.util.TerminalUtil;

public class UserUpdateProfileCommand extends AbstractUserCommand {

    private final String DESCRIPTION = "update profile of current user";

    private final String NAME = "update-user-profile";

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        final String userId = serviceLocator.getAuthService().getUserId();
        showParameterInfo("first name");
        final String firstName = TerminalUtil.nextLine();
        showParameterInfo("last name");
        final String lastName = TerminalUtil.nextLine();
        showParameterInfo("middle name");
        final String middleName = TerminalUtil.nextLine();
        serviceLocator.getUserService().updateUser(
                userId, firstName, lastName, middleName
        );
    }

    @Override
    public String name() {
        return NAME;
    }

}
