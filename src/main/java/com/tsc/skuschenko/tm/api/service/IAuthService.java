package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.User;

public interface IAuthService {

    User getUser();

    String getUserId();

    boolean isAuth();

    void login(String login, String password);

    void logout();

    User registry(String login, String password, String email);

}
