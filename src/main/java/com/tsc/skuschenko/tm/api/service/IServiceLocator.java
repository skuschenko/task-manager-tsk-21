package com.tsc.skuschenko.tm.api.service;

public interface IServiceLocator {

    void exit();

    IAuthService getAuthService();

    ICommandService getCommandService();

    IProjectService getProjectService();

    IProjectTaskService getProjectTaskService();

    ITaskService getTaskService();

    IUserService getUserService();

}
