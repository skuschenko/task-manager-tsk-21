package com.tsc.skuschenko.tm.api.repository;

import com.tsc.skuschenko.tm.model.Task;

public interface ITaskRepository extends IBusinessRepository<Task> {

}
