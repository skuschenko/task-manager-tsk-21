package com.tsc.skuschenko.tm.api.service;

import com.tsc.skuschenko.tm.model.Project;

public interface IProjectService extends IBusinessService<Project> {

    Project add(String name, String description);

}
